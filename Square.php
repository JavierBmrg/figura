<?php
# @Author: Edward F Bamberg
# @Date:   21-Sep-2017-22:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: Square.php
# @Last modified by:   Edward
# @Last modified time: 23-Sep-2017-19:09
# @License: gplv2

namespace Square;
use Shape\Shape;
use ShapeableInterfaces\Shapeable;
/**
 * Clase para Cuadrado
 */
class Square extends Shape implements Shapeable
{

  public function __construct()
  {

    $data = func_get_args();

    $this->name = $data[0][0];
    $this->base = $data[0][1];

    parent::__construct($this->name);
  }

  /**
   * @return int
   */
  public function getSurface()
  {
    return pow($this->base,2);
  }

  /**
   * @return string
   */
  public function getType()
  {
    return "Esta figura geométrica es un cuadrilátero regular";
  }

}
