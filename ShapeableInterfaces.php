<?php
# @Author: Edward F Bamberg
# @Date:   21-Sep-2017-22:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: ShapeActionable.php
# @Last modified by:   Edward
# @Last modified time: 23-Sep-2017-19:09
# @License: gplv2
namespace ShapeableInterfaces;


/**
 * Interface para metodos comunes de las clases Triangulo,Circulo,Cuadrado
 */
interface Shapeable
{
  public function getSurface();
}

/**
 * Interface para metodos comunes de la clase Triangulo.
 */
interface Triangleable
{
  public function getBase();
  public function getHeight();
}

/**
 * Interface para metodos comunes de la clase Circulo
 */
interface Circleable
{
  public function getDiameter();
}
