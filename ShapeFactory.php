<?php
namespace ShapeFactory;

# @Author: Edward F Bamberg
# @Date:   21-Sep-2017-22:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: ShapeFactory.php
# @Last modified by:   Edward
# @Last modified time: 23-Sep-2017-19:09
# @License: gplv2


use Triangle\Triangle;
use Square\Square;
use Circle\Circle;


/**
 * Clase Factory de Figuras
 */

class ShapeFactory
{

  public $shapeName;
  public $dataArray;

  public function __construct($data)
  {
    $this->dataArray = $data;
    $this->shapeName = $data[0];
  }

  public function createShape($shapeName)
  {

    switch (true) {
      case ('triangulo' == strtolower($shapeName) && count($this->dataArray) == 3):
        return new Triangle($this->dataArray);
        break;
      case ('circulo' == strtolower($shapeName) && count($this->dataArray) == 2):
        return new Circle($this->dataArray);
        break;
      case ('cuadrado' == strtolower($shapeName) && count($this->dataArray) == 2):
        return new Square($this->dataArray);
      break;
      default:
        self::raiseStaticError();
        break;
    }

  }

  private static function raiseStaticError()
  {
        return null;
  }

}
