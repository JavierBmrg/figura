<?php
# @Author: Edward F Bamberg
# @Date:   21-Sep-2017-23:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: Shape.php
# @Last modified by:   Edward
# @Last modified time: 23-Sep-2017-19:09
# @License: gplv2

namespace Shape;

 /**
 * Clase abstracta Figura
 */
abstract class Shape
{
    private $name;

    public function __construct($name)
    {
      $this->name = $name;
    }

    public function getName()
    {
      return $this->name;
    }

    abstract protected function getType();




}
