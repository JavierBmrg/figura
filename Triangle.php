<?php
# @Author: Edward F Bamberg
# @Date:   21-Sep-2017-22:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: Triangle.php
# @Last modified by:   Edward
# @Last modified time: 23-Sep-2017-19:09
# @License: gplv2
namespace Triangle;

use Shape\Shape;
use ShapeableInterfaces\Shapeable;
use ShapeableInterfaces\Triangleable;

/**
 * Clase para la figura triangulo.
 */
class Triangle extends Shape implements Shapeable, Triangleable
{

  public $base;
  public $height;
  const DENOMINATOR = 2;

  public function __construct()
  {

    $data = func_get_args();


    $this->name = $data[0][0];
    $this->base = $data[0][1];
    $this->height = $data[0][2];

    parent::__construct($this->name);
  }

  /**
   * @return int
   */
  public function getSurface()
  {
    return ($this->base * $this->height)/self::DENOMINATOR;
  }

  /**
   *@return int
   */
  public function getBase()
  {
    return $this->base;
  }

  /**
   * @return int
   */
  public function getHeight()
  {
    return $this->height;
  }

  /**
   * @return string
   */
  public function getType()
  {
    return "Esta figura es un polígono delimitado por tres lados, tres vertices y tres ángulos.";
  }

}
