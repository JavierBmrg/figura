<?php
# @Author: Edward F Bamberg
# @Date:   21-Sep-2017-22:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: Circle.php
# @Last modified by:   Edward
# @Last modified time: 23-Sep-2017-19:09
# @License: gplv2
namespace Circle;
use Shape\Shape;
use ShapeableInterfaces\Shapeable;
use ShapeableInterfaces\Circleable;
/**
 * Clase para Circulo
 */
class Circle extends Shape implements Shapeable, Circleable
{

  const MULT = 2;

  public function __construct()
  {

    $data = func_get_args();

    $this->name = $data[0][0];
    $this->radio = $data[0][1];

    parent::__construct($this->name);
}

/**
 * @return int
 */
  public function getSurface()
  {
    return round(pow($this->radio,2)*M_PI,2);
  }
  /**
   * @return int
   */
  public function getDiameter()
  {
    return $this->radio*self::MULT;
  }
  /**
   * @return string
   */
  public function getType()
  {
    return "Esta figura geométrica es plana (bidimensional) delimitada por un diámetro.";
  }


}
