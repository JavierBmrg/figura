<?php
namespace Root;
include_once 'includes.inc';
# @Author: Edward F Bamberg
# @Date:   21-Sep-2017-22:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: index.php
# @Last modified by:   Edward
# @Last modified time: 23-Sep-2017-20:09
# @License: gplv2
use ShapeFactory\ShapeFactory;

/**
 * Archivo init, a partir de aquí y en adelante para gestionar las clases manda la creatividad
 */


$dataShape = ['circulo',2];


$instance = new ShapeFactory($dataShape);

echo $instance->createShape($dataShape[0])->getName();
echo $instance->createShape($dataShape[0])->getType();
echo $instance->createShape($dataShape[0])->getSurface();
echo $instance->createShape($dataShape[0])->getDiameter();
