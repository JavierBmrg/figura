<?php
# @Author: Javier Bamberg <javier>
# @Date:   23-Sep-2017-14:09
# @Email:  jaavfnz@gmail.com
# @Project: !projectName
# @Filename: include.inc
# @Last modified by:   Edward
# @Last modified time: 23-Sep-2017-19:09
# @License: gplv2

/**
 * Inclusión de dependencias.
 */

include 'ShapeFactory.php';
include 'Shape.php';
include 'ShapeableInterfaces.php';
include 'Triangle.php';
include 'Square.php';
include 'Circle.php';
